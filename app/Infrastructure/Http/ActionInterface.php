<?php

declare(strict_types=1);

namespace App\Infrastructure\Http;

use Illuminate\Http\JsonResponse;

interface ActionInterface
{
    public function __invoke(): JsonResponse;
}
