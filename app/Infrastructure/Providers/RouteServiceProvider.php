<?php

declare(strict_types=1);

namespace App\Infrastructure\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * Typically, users are redirected here after authentication.
     *
     * @var string
     */
    public const HOME = '/home';

    protected const BASE_MODULES_PATH = 'app/Modules';
    protected const MODULE_API_ROUTES_PATH = 'Infrastructure/routes/api.php';

    protected array $modules = [
        'invoices',
    ];

    /**
     * Define your route model bindings, pattern filters, and other route configuration.
     *
     */
    public function boot(): void
    {
        $this->configureRateLimiting();

        $this->routes(function (): void {
            Route::middleware('api')
                ->prefix('api')
                ->group(base_path('routes/api.php'));

            Route::middleware('web')
                ->group(base_path('routes/web.php'));
        });

        $this->registerModuleApiRoutes();
    }

    /**
     * Configure the rate limiters for the application.
     *
     */
    protected function configureRateLimiting(): void
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by($request->user()?->id ?: $request->ip());
        });
    }

    protected function registerModuleApiRoutes(): void
    {
        foreach ($this->modules as $module) {
            $path = sprintf(
                '%s/%s/%s',
                self::BASE_MODULES_PATH,
                Str::ucfirst($module),
                self::MODULE_API_ROUTES_PATH
            );

            $prefix = sprintf('%s/%s', 'api', $module);

            Route::prefix($prefix)
                ->middleware('api')
                ->group(base_path($path));
        }
    }
}
