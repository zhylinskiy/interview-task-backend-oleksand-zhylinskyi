<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Infrastructure\routes;

use App\Modules\Invoices\Api\Http\Controllers\InvoiceController;
use Illuminate\Support\Facades\Route;

Route::get('/', [InvoiceController::class, 'index']);
Route::get('/{id}', [InvoiceController::class, 'show']);
Route::post('/{id}/approve', [InvoiceController::class, 'approve']);
Route::post('/{id}/reject', [InvoiceController::class, 'reject']);
