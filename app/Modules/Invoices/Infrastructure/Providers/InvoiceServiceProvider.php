<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Infrastructure\Providers;

use Illuminate\Support\ServiceProvider;

class InvoiceServiceProvider extends ServiceProvider
{
    public function register()
    {

    }
}
