<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Domain\Repositories;

use App\Modules\Invoices\Api\Dto\GetPaginatedDto;
use App\Modules\Invoices\Domain\Models\Invoice;
use Illuminate\Pagination\LengthAwarePaginator;

class InvoiceRepository
{
    public const PAGE_DEFAULT = 1;
    public const PER_PAGE_DEFAULT = 10;
    public const PER_PAGE_MAX = 50;

    public function getPaginated(GetPaginatedDto $dto): LengthAwarePaginator
    {
        return Invoice::query()->paginate(perPage: $dto->per_page, page: $dto->page);
    }

    public function getById(string $id): ?Invoice
    {
        /** @var Invoice|null $invoice */
        $invoice = Invoice::query()
            ->where('id', $id)
            ->with(['company', 'billedCompany', 'products'])
            ->first();

        return $invoice;
    }

    public function updateStatus(string $id, string $status): void
    {
        Invoice::query()
            ->where('id', $id)
            ->update(['status' => $status]);
    }
}
