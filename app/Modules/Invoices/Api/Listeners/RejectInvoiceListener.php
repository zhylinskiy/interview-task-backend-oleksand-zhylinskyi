<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api\Listeners;

use App\Domain\Enums\StatusEnum;
use App\Modules\Invoices\Domain\Repositories\InvoiceRepository;
use App\Modules\Invoices\Modules\Approval\Api\Events\EntityRejected;

final readonly class RejectInvoiceListener
{
    public function __construct(private InvoiceRepository $repository)
    {
    }

    public function handle(EntityRejected $event): void
    {
        $this->repository->updateStatus(
            $event->approvalDto->id->toString(),
            StatusEnum::REJECTED->value
        );
    }
}
