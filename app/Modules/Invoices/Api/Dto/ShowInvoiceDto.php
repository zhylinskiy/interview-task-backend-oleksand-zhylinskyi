<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api\Dto;

use App\Modules\Invoices\Api\Dto\Casters\UuidCaster;
use Illuminate\Http\Request;
use Ramsey\Uuid\UuidInterface;
use WendellAdriel\ValidatedDTO\SimpleDTO;

class ShowInvoiceDto extends SimpleDTO
{
    public ?UuidInterface $id;

    public function casts(): array
    {
        return [
            'id' => new UuidCaster(),
        ];
    }

    public function defaults(): array
    {
        return [
            'id' => null,
        ];
    }

    public static function fromRequest(Request $request): static
    {

        return new static([
            'id' => $request->route('id'),
        ]);
    }
}
