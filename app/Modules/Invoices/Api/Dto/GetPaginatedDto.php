<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api\Dto;

use App\Modules\Invoices\Domain\Repositories\InvoiceRepository;
use WendellAdriel\ValidatedDTO\Casting\IntegerCast;
use WendellAdriel\ValidatedDTO\ValidatedDTO;

class GetPaginatedDto extends ValidatedDTO
{
    public int $page;

    public int $per_page;

    protected function rules(): array
    {
        $min = InvoiceRepository::PER_PAGE_DEFAULT;
        $max = InvoiceRepository::PER_PAGE_MAX;

        return [
            'page' => ['sometimes', 'required', 'integer'],
            'per_page' => [
                'sometimes',
                'required',
                'integer',
                "min:$min",
                "max:$max",
            ],
        ];
    }

    protected function defaults(): array
    {
        return [
            'page' => InvoiceRepository::PAGE_DEFAULT,
            'per_page' => InvoiceRepository::PER_PAGE_DEFAULT,
        ];
    }

    protected function casts(): array
    {
        return [
            'page' => new IntegerCast(),
            'per_page' => new IntegerCast(),
        ];
    }
}
