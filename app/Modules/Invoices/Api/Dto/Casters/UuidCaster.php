<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api\Dto\Casters;

use App\Modules\Invoices\Api\Exceptions\InvoiceNotFoundException;
use Ramsey\Uuid\Exception\InvalidUuidStringException;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use WendellAdriel\ValidatedDTO\Casting\Castable;

class UuidCaster implements Castable
{
    /**
     * @throws InvoiceNotFoundException
     */
    public function cast(string $property, mixed $value): UuidInterface
    {
        try {
            return Uuid::fromString($value);
        } catch (InvalidUuidStringException $e) {
            throw new InvoiceNotFoundException();
        }
    }
}
