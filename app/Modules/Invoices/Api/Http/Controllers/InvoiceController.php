<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api\Http\Controllers;

use App\Infrastructure\Http\Controller;
use App\Modules\Invoices\Api\Dto\GetPaginatedDto;
use App\Modules\Invoices\Api\Dto\ShowInvoiceDto;
use App\Modules\Invoices\Api\Http\Actions;
use App\Modules\Invoices\Api\Http\Resources\InvoiceResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Validation\ValidationException;
use WendellAdriel\ValidatedDTO\Exceptions\CastTargetException;
use WendellAdriel\ValidatedDTO\Exceptions\MissingCastTypeException;

class InvoiceController extends Controller
{
    /**
     * @throws CastTargetException
     * @throws MissingCastTypeException
     * @throws ValidationException
     */
    public function index(Request $request, Actions\Index $action): AnonymousResourceCollection
    {
        $dto = GetPaginatedDto::fromRequest($request);

        return InvoiceResource::collection($action($dto));
    }

    /**
     * @throws CastTargetException
     * @throws MissingCastTypeException
     * @throws ValidationException|\App\Modules\Invoices\Api\Exceptions\InvoiceNotFoundException
     */
    public function show(Request $request, Actions\Show $action): InvoiceResource
    {
        $dto = ShowInvoiceDto::fromRequest($request);

        return new InvoiceResource($action($dto));
    }

    /**
     * @throws CastTargetException
     * @throws MissingCastTypeException
     * @throws ValidationException
     */
    public function approve(Request $request, Actions\Approve $action): JsonResponse
    {
        $dto = ShowInvoiceDto::fromRequest($request);
        $action($dto);

        return new JsonResponse(['success' => true]);
    }

    /**
     * @throws CastTargetException
     * @throws MissingCastTypeException
     * @throws ValidationException
     */
    public function reject(Request $request, Actions\Reject $action): JsonResponse
    {
        $dto = ShowInvoiceDto::fromRequest($request);
        $action($dto);

        return new JsonResponse(['success' => true]);
    }
}
