<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api\Http\Actions;

use App\Domain\Enums\StatusEnum;
use App\Modules\Invoices\Domain\Models\Invoice;
use App\Modules\Invoices\Api\Dto\ShowInvoiceDto;
use App\Modules\Invoices\Modules\Approval\Api\Dto\ApprovalDto;
use App\Modules\Invoices\Domain\Repositories\InvoiceRepository;
use App\Modules\Invoices\Api\Exceptions\InvoiceNotFoundException;
use App\Modules\Invoices\Modules\Approval\Application\ApprovalFacade;
use App\Modules\Invoices\Modules\Approval\Application\Exceptions\StatusAlreadyAssignedException;

final readonly class Approve
{
    public function __construct(
        private InvoiceRepository $repository,
        private ApprovalFacade $approvalFacade
    ) {
    }

    /**
     * @throws InvoiceNotFoundException
     * @throws StatusAlreadyAssignedException
     */
    public function __invoke(ShowInvoiceDto $dto): void
    {
        $invoice = $this->repository->getById($dto->id->toString());

        if (is_null($invoice)) {
            throw new InvoiceNotFoundException();
        }

        $this->approvalFacade->approve(
            new ApprovalDto(
                $dto->id,
                StatusEnum::tryFrom($invoice->status),
                Invoice::ENTITY_NAME
            )
        );
    }
}
