<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api\Http\Actions;

use App\Modules\Invoices\Api\Dto\GetPaginatedDto;
use App\Modules\Invoices\Domain\Repositories\InvoiceRepository;
use Illuminate\Pagination\LengthAwarePaginator;

final readonly class Index
{
    public function __construct(
        private InvoiceRepository $repository
    ) {
    }

    public function __invoke(GetPaginatedDto $dto): LengthAwarePaginator
    {
        return $this->repository->getPaginated($dto);
    }
}
