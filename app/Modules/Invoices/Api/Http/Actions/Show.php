<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api\Http\Actions;

use App\Modules\Invoices\Domain\Models\Invoice;
use App\Modules\Invoices\Api\Dto\ShowInvoiceDto;
use App\Modules\Invoices\Domain\Repositories\InvoiceRepository;
use App\Modules\Invoices\Api\Exceptions\InvoiceNotFoundException;

final readonly class Show
{
    public function __construct(
        private InvoiceRepository $repository
    ) {
    }

    /**
     * @throws InvoiceNotFoundException
     */
    public function __invoke(ShowInvoiceDto $dto): Invoice
    {
        $invoice = $this->repository->getById($dto->id->toString());

        if (is_null($invoice)) {
            throw new InvoiceNotFoundException();
        }

        return $invoice;
    }
}
