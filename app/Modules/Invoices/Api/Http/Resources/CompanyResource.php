<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CompanyResource extends JsonResource
{
    public function toArray($request): array
    {
        return parent::toArray($request);
    }
}
