<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Modules\Approval\Application\Exceptions;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class StatusAlreadyAssignedException extends \Exception
{
    public function render(): JsonResponse
    {
        return new JsonResponse(['message' => 'approval status is already assigned'], Response::HTTP_BAD_REQUEST);
    }
}
