<?php

declare(strict_types=1);

namespace Tests\Feature\Invoice;

use App\Domain\Enums\StatusEnum;
use App\Modules\Invoices\Domain\Models\Company;
use App\Modules\Invoices\Domain\Models\Invoice;
use Illuminate\Http\Response;
use Tests\TestCase;

class ApproveTest extends TestCase
{
    public function test_approve_invoice(): void
    {
        /** @var Company $company */
        $company = Company::factory()->create();
        /** @var Company $billedCompany */
        $billedCompany = Company::factory()->create();

        /** @var Invoice $invoice */
        $invoice = Invoice::factory()
            ->create([
                'status' => StatusEnum::DRAFT->value,
                'company_id' => $company->id,
                'billed_company_id' => $billedCompany->id,
            ]);

        $response = $this->post(sprintf('/api/invoices/%s/approve', $invoice->id));

        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseHas('invoices', [
            'id' => $invoice->id,
            'status' => StatusEnum::APPROVED->value,
        ]);
    }

    public function test_reject_invoice(): void
    {
        /** @var Company $company */
        $company = Company::factory()->create();
        /** @var Company $billedCompany */
        $billedCompany = Company::factory()->create();

        /** @var Invoice $invoice */
        $invoice = Invoice::factory()
            ->create([
                'status' => StatusEnum::DRAFT->value,
                'company_id' => $company->id,
                'billed_company_id' => $billedCompany->id,
            ]);

        $response = $this->post(sprintf('/api/invoices/%s/reject', $invoice->id));

        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseHas('invoices', [
            'id' => $invoice->id,
            'status' => StatusEnum::REJECTED->value,
        ]);
    }
}
