<?php

declare(strict_types=1);

namespace Tests\Feature\Invoice;

use App\Modules\Invoices\Domain\Models\Company;
use App\Modules\Invoices\Domain\Models\Invoice;
use Illuminate\Http\Response;
use Tests\TestCase;

class IndexTest extends TestCase
{
    public function test_index_endpoint(): void
    {
        $company = Company::factory()->create();
        $billedCompany = Company::factory()->create();

        Invoice::factory()
            ->for($company)
            ->for($billedCompany)
            ->count(20)
            ->create();

        $response = $this->get('/api/invoices?per_page=15');

        $response->assertStatus(Response::HTTP_OK)
            ->assertJsonCount(15, 'data');
    }
}
